import os

from flask import Flask, render_template, request, redirect, session, flash

from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

# Config BDD

project_dir = os.path.dirname(os.path.abspath(__file__))
uri_database = "sqlite:///{}".format(os.path.join(project_dir, "myApp.db"))
app.config["SQLALCHEMY_DATABASE_URI"] = uri_database
app.config["SQLALCHEMY_BINDS"] = {
    'users': "sqlite:///users.db",
    'books': "sqlite:///books.db"
}
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

# Modèles pour nos deux bases de données

class User(db.Model):
    __bind_key__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80))
    password = db.Column(db.String(80))

    def __init__(self, username, password):
        self.username = username
        self.password = password

class Book(db.Model):
    __bind_key__ = 'books'

    title = db.Column(db.String(80), unique=True, nullable=False, primary_key=True)

    def __init__(self, title):
        self.title = title

# Route de base

@app.route("/", methods=["GET", "POST"])
def index():
    return render_template("index.html")

# Toutes les routes concernant l'authentification

@app.route("/login", methods=["GET", "POST"])
def login():
    try:
        name = request.form['username']
        passw = request.form['password']
        data = User.query.filter_by(username=name, password=passw).first()
        if data is not None:
            session['logged_in'] = True
            books = Book.query.all()
            return render_template("home.html", books=books)
        else:
            return redirect("/")
    except:
        return redirect("/")

@app.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "GET":
        return render_template("register.html")
    else:
        try:
            toInsert = User(username = request.form['username'], password=request.form['password'])
            db.session.add(toInsert)
            db.session.commit()
            session['logged_in'] = True
            books = Book.query.all()
            return render_template("home.html", books=books)
        except:
            return render_template("register.html")


@app.route("/logout", methods=["GET", "POST"])
def logout():
    session['logged_in'] = False
    return redirect("/")

# Toutes les routes pour le CRUD des livres

@app.route("/home", methods=["GET", "POST"])
def home():
    books = None
    if request.form:
        try:
            book = Book(title=request.form.get("title"))
            db.session.add(book)
            db.session.commit()
        except Exception as e:
            print("Failed to add book")
            print(e)
    books = Book.query.all()
    return render_template("home.html", books=books)

@app.route("/update", methods=["POST"])
def update():
    try:
        newtitle = request.form.get("newtitle")
        oldtitle = request.form.get("oldtitle")
        book = Book.query.filter_by(title=oldtitle).first()
        book.title = newtitle
        db.session.commit()
    except Exception as e:
        print("Couldn't update book title")
        print(e)
    return redirect("/home")

@app.route("/delete", methods=["POST"])
def delete():
    title = request.form.get("title")
    book = Book.query.filter_by(title=title).first()
    db.session.delete(book)
    db.session.commit()
    return redirect("/home")

# Démarrage de l'app

if __name__ == "__main__":
    #Création des dbs
    db.drop_all()
    db.create_all()

    #Insertion de quelques dummies values
    toto = User("toto", "toto")
    tata = User("tata", "tata")
    lotr = Book("Le seigneur des anneaux")
    db.session.add(toto)
    db.session.add(tata)
    db.session.add(lotr)
    db.session.commit()

    #Clef secrète pour le login
    app.secret_key = "123"

    app.run(host='127.0.0.1', port=8080, debug=True)
