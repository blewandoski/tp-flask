myCRUD Template
==============

Ce tp consiste à réaliser une simple application web de gestion de livres.

Informations générales
============

    Ce tp a été conçu à l'aide d'un environnement de travail linux et son bon déroulement a donc été vérifié sur cet OS uniquement. Nous vous conseillons d'utiliser une VM si vous n'avez pas de linux. Néanmoins, sachez qu'importer un projet flask sous windows est possible en utilisant pycharm qui appartient à JetBrains (https://www.jetbrains.com/help/pycharm/creating-flask-project.html).

Pré-requis
============

    $ sudo apt-get install git virtualenv python3-virtualenv python3-pip sqlitebrowser
    $ git clone https://gitlab.com/blewandoski/tp-flask.git
    $ cd tp-flask
    $ virtualenv --python=/usr/bin/python3 venv
    $ source venv/bin/activate
    $ pip3 install -r requirements.txt

Déroulement du TP
===========

    Vous pouvez suivre les instructions contenues dans le PDF

Exécution
===========

    $ python3 myApp.py

Liens Utiles
===========

    https://docs.python.org/fr/3

    https://flask.palletsprojects.com/en/1.1.x/

    https://flask.palletsprojects.com/en/1.1.x/quickstart/
